package com.example.springbeangradle;

import com.example.springbeangradle.service.UserService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

@SpringBootApplication
public class SpringBeanGradleApplication {

	public static void main(String[] args) {
		// SpringApplication.run(SpringBeanGradleApplication.class, args);
		ApplicationContext ctx = new AnnotationConfigApplicationContext(SpringBeanGradleApplication.class);
		// By its type
		// UserService userService = ctx.getBean(UserService.class);
		// By its alias name
		UserService userService = (UserService) ctx.getBean("myBean");
		System.out.println("Printing user service::" + userService.getList());

	}

}
